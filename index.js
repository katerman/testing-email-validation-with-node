const { readJson } = require('fs-extra');
const { validate } = require('isemail');

(async () => {
  const regexJson = await readJson('./regex.json');
  const sampleRegexJson = await readJson('./sampleRegex.json');
  const regex = new RegExp(sampleRegexJson[regexJson.regex] || regexJson.regex);

  const resultData = { falsePositive: 0, falseNegative: 0, total: 0 };
  const { tests } = await readJson('./emaildata.json');

  tests.forEach(({ address }) => {
    const ourRegexValidated = regex.test(address);
    const isEmailValidated = validate(address);

    // inc test amount
    resultData.total += 1;

    if (ourRegexValidated && !isEmailValidated) {
      // false positive
      resultData.falsePositive += 1;
      console.log(`Email address: ${address} passed but should have failed`);
    }

    if (!ourRegexValidated && isEmailValidated) {
      // false negative
      resultData.falseNegative += 1;
      console.log(`Email address: ${address} failed but should have passed`);
    }
  });

  console.log(`There were ${resultData.total} tests run.`);
  console.log(`There were ${resultData.falsePositive} false positives.`);
  console.log(`There were ${resultData.falseNegative} false negatives.`);
})();
