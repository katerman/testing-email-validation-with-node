# Testing email validation with node

## Goal
This small project aims to prove out how hard it is to really test against valid emails using a few common regex patterns.

## Thanks to
http://isemail.info/ for the list of valid email addresses
